FROM node:12

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install && npm install -g nodemon

COPY ./src ./src

EXPOSE 4000

CMD [ "node", "src/index.js" ]
