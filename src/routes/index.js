var router = require('express').Router();

router.use('/user', require('../controller/user.controller'));
router.use('/product', require('../controller/product.controller'));

module.exports = router;
