var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var {v4 : uuidv4} = require('uuid')
var Product = mongoose.model('Product');
var User = mongoose.model('User');

var shuffle = (array) => array.sort(() => Math.random() - 0.5);

router.get("/recommendations", (req, res) => {
  if (req.query.userId) {
    User.findOne({userId: req.query.userId})
      .exec()
      .then((user) => {
        var preferencesList = []
        for (const [key, value] of Object.entries(user.preferences)) {
          if(value) {
            preferencesList.push(key);
          }
        }
        Product.find()
          .exec()
          .then((products) => {
            var result = [];

            for (var i in products) {
              var product = products[i]

              if (product.preferences.includes('gluten_free')) {
                  product.addImageUrl()
                  result.push(product);
              }

              // var flaggedPreferencesList = Array.from(preferencesList)
              // [gluten_free, cruelty_free] - [gluten_free] = [cruelty_free]
              // flaggedPreferencesList = flaggedPreferencesList.filter(( el ) => !product.preferences.includes( el ))
              // console.log(`${preferencesList} - ${product.preferences} = ${flaggedPreferencesList}` )
              // product.flaggedPreferences = flaggedPreferencesList
              product.addImageUrl()
            }

            result = shuffle(result)
            res.send(result)
          });
      })
  } else {
    Product.find()
      .exec()
      .then((products) => {
        for (var i in products) {
          var product = products[i]
          product.addImageUrl()
        }
        res.send(products)
      });
  }
})

router.get("/filtered-recommendations", (req, res) => {
  if (req.query.userId) {
    User.findOne({userId: req.query.userId})
      .exec()
      .then((user) => {
        var preferencesList = []
        for (const [key, value] of Object.entries(user.preferences)) {
          if(value) {
            preferencesList.push(key);
          }
        }
        Product.find()
          .exec()
          .then((products) => {
            var filteredProducts = []
            for (var i in products) {
              var product = products[i]
              var flaggedPreferenciesList = Array.from(product.preferences)
              if (product.preferences)
              flaggedPreferenciesList = flaggedPreferenciesList.filter(( el ) => preferencesList.includes( el ))
              if (!flaggedPreferenciesList.length > 0) {
                product.addImageUrl()
                filteredProducts.push(product)
              }
            }
            res.send(filteredProducts)
          });
      })
  } else {
    Product.find()
      .exec()
      .then((products) => {
        res.send(products)
      });
  }
})

router.get("/recommendation-category", (req, res) => {
  Product.find({productType: "shampoo"})
    .exec()
    .then((product) => {
      addImageUrl(product)
      res.send(product)
    });
})

router.get("/recommendation-category-filtered", (req, res) => {
  Product.find({productType: "shampoo"})
    .exec()
    .then((products) => {
      var filtered = []
      for (var i in products) {
        var product = products[i];
        if (!product.flaggedPreferencies.length > 0) {
          addImageUrl(product)
          filtered.push(product)
        }
      }
      res.send(filtered)
    });
})

router.get("/:productId", (req, res) => {
  Product.findById(req.params.productId)
    .exec()
    .then((product) => {
      product.addImageUrl();
      res.send(product)
    });
})

router.get("/", (req, res) => {
  Product.find()
    .exec()
    .then((products) => {
      for (var i in products) {
        var product = products[i]
        product.addImageUrl()
      }
      res.send(products)
    });
})

function addImageUrl (product) {
  product.image_url = 'http://junction-api.pjtuxe.com/static/formatted/' + product._id + '/Artboard 1.jpg';
}

module.exports = router;
