var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var {v4 : uuidv4} = require('uuid')
var User = mongoose.model('User');

router.get("/", (req, res) => {
  User.findOne({userId: req.query.userId}).exec().then((user) => {
    if (user) {
      res.send(user)
    } else {
      var newUser = new User({email: "example@gmail.com", userId: req.query.userId})
      newUser.save(function(err, doc) {
        if (err) return console.error(err);
      })
      return res.send(newUser)
    }
  });
})

router.get("/:userId", (req, res) => {
  User.findOne({userId: req.params.userId}).exec().then((user) => {
    if (user) {
      res.send(user)
    } else {
      var newUser = new User({email: "example@gmail.com", userId: req.params.userId})
      newUser.save(function(err, doc) {
        if (err) return console.error(err);
      })
      return res.send(newUser)
    }
  });
})

router.post("/login", (req, res) => {
  User.findOne({email: req.body.email}).exec().then(function(user){
    if(!user){
      var newUser = new User({email: req.body.email, userId: uuidv4()})
      newUser.save(function(err, doc) {
        if (err) return console.error(err);
      })
      return res.send(newUser)
    }
    res.send({user});
  })
})

router.put("/preferences", (req, res) => {
  User.findOne({userId: req.body.userId}).exec().then(function(user, err) {
    if (!user)
      res.send(err);
    user.preferences = req.body.preferences;
    user.save(function(err) {
      if (err)
        res.send(err);
      res.send(user);
    });
  })
})

module.exports = router;
