var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
  userId: String,
  email: String,
  preferences: Object
})

mongoose.model("User", UserSchema, "users");
