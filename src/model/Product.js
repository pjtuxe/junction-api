var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
  _id: String,
  title: String,
  description: String,
  kind: String,
  availability: String,
  brand: String,
  color: String,
  gender: String,
  price: Number,
  productType: String,
  sizes: String,
  ingredients: [],
  preferences: [],
  image_url: String,
  flaggedPreferencies: []
})

ProductSchema.methods.addImageUrl = function() {
  this.image_url = 'http://junction-api.pjtuxe.com/static/formatted/' + this._id + '/Artboard 1.jpg';
};

mongoose.model("Product", ProductSchema, "products");
