const path = require("path")
const express = require("express")
const app = express()
const cors = require("cors")
const mongoose = require('mongoose')

app.use(cors())
app.options("*", cors())

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.use("/static", express.static('src/static'))

mongoose.connect("mongodb://34.89.20.95:9999/shopility");

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('./model/User');
require('./model/Product');
app.use('/api', require('./routes'));

app.listen(4000, () => {

    console.log("App listening on 0.0.0.0:4000...")
})
